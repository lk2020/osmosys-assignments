var file_names = ["song1.mp3", "song2.mp3", "song3.mp3", "song4.mp3", "song5.mp3", "movie1.mkv", "movie2.mkv", "movie3.mkv", "movie4.mkv", "img1.jpg", "img2.jpg", "img3.jpg", "img4.jpg", "file1.pdf", "file2.pdf", "file3.pdf", "file4.pdf"];
var json_obj = {
    audio: [],
    video: [],
    image: [],
    document: []
}
function convertToJsonObject(file_names, obj) {
    var audio_file = /[a-z1-9]+[.]mp3$/;
    var video_file = /[a-z1-9]+[.]mkv$/;
    var image_file = /[a-z1-9]+[.]jpg$/;
    var document_file = /[a-z1-9]+[.]pdf$/;
    file_names.forEach((e) => {
        if (audio_file.test(e)) {
            obj.audio.push(e);
        }
        if (video_file.test(e)) {
            obj.video.push(e);
        }
        if (image_file.test(e)) {
            obj.image.push(e);
        }
        if (document_file.test(e)) {
            obj.document.push(e);
        }
    });
    console.log(obj);
}
convertToJsonObject(file_names, json_obj);