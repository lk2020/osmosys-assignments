function birthday() {
    current_date = new Date();
    birth_dates = [
        {
            name: "asish",
            birth_date: new Date(1998, 06, 23)
        },
        {
            name: "rajeev",
            birth_date: new Date(1998, 05, 25)
        },
        {
            name: "manohar",
            birth_date: new Date(1997, 06, 23)
        },
        {
            name: "rocky",
            birth_date: new Date(1998, 03, 22)
        },
        {
            name: "rajesh",
            birth_date: new Date(1998, 07, 13)
        },
        {
            name: "arpita",
            birth_date: new Date(1998, 09, 3)
        },
        {
            name: "raghav",
            birth_date: new Date(1998, 06, 21)
        },
        {
            name: "vivek",
            birth_date: new Date(1998, 06, 12)
        }
    ]
    localStorage.setItem("birthdates", JSON.stringify(birth_dates));
    let number_of_day_for_upcoming_birthday;
    //logic for finding the birthdays in next month
    function nextMonthBirthday() {
        let nextmonth_birthday = [];
        for (let date in birth_dates) {
            if (birth_dates[date].birth_date.getMonth() - current_date.getMonth() == 1) {
                nextmonth_birthday.push(birth_dates[date].name);
            }
        }
        console.log(`people those who are having birthday next month are ${nextmonth_birthday}`);
        localStorage.setItem("nextmonth_birthday", nextmonth_birthday);
    }
    //logic for finding the birthdays on sunday
    function birthDayOnSunday() {
        let birthdayon_sunday = [];
        for (let date in birth_dates) {
            if (birth_dates[date].birth_date.getDay() == 0) {
                birthdayon_sunday.push(birth_dates[date].name);
            }
        }
        console.log(`The people whose birthday on sunday are ${birthdayon_sunday}`);
        localStorage.setItem("birthday on sunday", birthdayon_sunday);
    }
    //logic for finding the birthdays on the same day
    function birthDayOnSameDay() {
        for (let date in birth_dates) {
            var sameday_birthday = [];
            if (birth_dates[date].birth_date.getDay() == current_date.getDay()) {
                sameday_birthday.push(birth_dates[date].name);
            }
        }
        console.log(`people whose birthday is on the same day are ${sameday_birthday}`);
        localStorage.setItem("birthday on sameday", sameday_birthday);
    }
    //logic for calculating the number of days for the upcoming birthday
    function numberOfDays() {
        let upcoming_birthdays = [];
        for (let date of birth_dates) {
            if (date.birth_date.getMonth() > current_date.getMonth()) {
                upcoming_birthdays.push(date);
            }
        }

        let nearest_month_objs = []
        let min = upcoming_birthdays[0].birth_date.getMonth()
        for (let date of upcoming_birthdays) {
            if (date.birth_date.getMonth() <= min) {
                nearest_month_objs.push(date);
            }
        }
        let nearest_date;
        let min2 = nearest_month_objs[0].birth_date.getDate();
        for (let day of upcoming_birthdays) {
            if (day.birth_date.getDate() <= min2) {
                nearest_date = day;
            }
        }
        let number_of_milliseconds = (new Date(current_date.getFullYear(), nearest_date.birth_date.getMonth(), nearest_date.birth_date.getDate())) - current_date;
        let number_of_days = Math.floor(number_of_milliseconds / (1000 * 60 * 60 * 24));
        console.log(`Total number of days for the upcoming birthday is ${number_of_days}`);
        localStorage.setItem("number of days for the upcoming birthday", number_of_days);
    }
    //writing the logic for finding out the oldest friend
    function oldestFriend() {
        max = current_date - birth_dates[0].birth_date;
        let oldest_friend;
        for (let date of birth_dates) {
            if ((current_date - date.birth_date) >= max) {
                oldest_friend = date.name;
                max = current_date - date.birth_date;
            }
        }
        console.log(`The name of the oldest friend is ${oldest_friend}`);
        localStorage.setItem("oldestfriend", oldest_friend);
    }
    function addFriend(frnd) {
        let item = JSON.parse(localStorage.getItem("birthdates"));
        item.push(frnd);
        localStorage.setItem("birthdates", JSON.stringify(item));
    }
    return {
        nextmonth: nextMonthBirthday,
        birthDayOnSunday: birthDayOnSunday,
        birthDayOnSameDay: birthDayOnSameDay,
        numberOfDays: numberOfDays,
        oldestFriend: oldestFriend,
        addFriend: addFriend
    }
}
var obj = new birthday();
obj.nextmonth();
obj.birthDayOnSunday();
obj.birthDayOnSameDay();
obj.numberOfDays();
obj.oldestFriend();
var frnd = {
    name: "akash",
    birth_date: new Date(1998, 08, 22)
}
obj.addFriend(frnd);