var question_obj = [
    {
        question: "click on the circle shape object",
        type: "circle"
    },
    {
        question: "click on the rectangle shape object",
        type: "rectangle"
    },
    {
        question: "click on the triangle shape object",
        type: "triangle"
    },
    {
        question: "click on the square shape object",
        type: "square"
    },
    {
        question: "click on a cloud",
        type: "cloud"
    },
    {
        question: "click on a flower plant",
        type: "flower"
    },
    {
        question: "click on a tree",
        type: "tree"
    },
    {
        question: "click on the crow",
        type: "crow"
    }
];
function handleUitasks(obj) {
    var questions = [];
    var types = [];
    var j = 0;
    var status = "true";
    obj.forEach((obj1) => {
        for (i in obj1) {
            if (i == "question") {
                questions.push(obj1[i]);
            } else {
                types.push(obj1[i]);
            }
        }
    });
    changeThequestions(questions[j], status);
    var $container = $("#image-container");
    $container.click((e) => {
        if (e.target.id == types[j]) {
            j += 1;
            status = "true";
            changeThequestions(questions[j], status);
        } else {
            status = "false";
            changeThequestions(questions[j], status);
        }
        if (j == types.length) {
            j = 0;
            changeThequestions(questions[j], status);
        }
    });
}
function changeThequestions(question, status) {
    //removing the old div
    //creating the new div
    //appending the new di
    if (status == "true") {
        let del = $(".current");
        del.remove();
        let $newquestion = $("<div class=current > " + question + "</div >")
        let $img = $("#image-container");
        $newquestion.insertBefore($img);
    }
    if (status == "false") {
        let del = $(".current");
        del.remove();
        let $newquestion = $("<div class=current>try again </div >")
        let $img = $("#image-container");
        $newquestion.insertBefore($img);
    }
}
handleUitasks(question_obj);