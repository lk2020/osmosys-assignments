function circleConfig() {
    var circleconfig = [
        {
            score: -2,
            className: "red-circle"
        },
        {
            score: -1,
            className: "violet-circle"
        },
        {
            score: 3,
            className: "green-circle"
        },
        {
            score: 2,
            className: "yellow-circle"
        },
        {
            score: 1,
            className: "blue-circle"
        }
    ];
    return {
        scoredata: circleconfig
    }
}