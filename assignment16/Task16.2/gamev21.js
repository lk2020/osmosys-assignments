function clickBasedGame() {
    var timelimit;
    var timeInterval;
    var randomCirclegenerateInterval;
    var circleconfig = circleConfig().scoredata;
    var score = 0;
    var playareaheight;
    var playareawidth;
    var boxappearingtime;
    var borderRadius;
    $("#difficulty-section").click(function (e) {
        setPlayArea(e.target);
    });
    $("#shape-section").click((e) => {
        setShape(e.target.id)
    });
    $("#duration-btn").click(function () {
        timelimit = $("#duration").val();
    });
    function setShape(target) {
        if (target === "circle") {
            borderRadius = 50;
        }
        if (target === "square") {
            borderRadius = 0;
        }
    }
    function setPlayArea(target) {
        if (target.id === "easy") {
            playareaheight = 480;
            playareawidth = 600;
            boxappearingtime = 1200
        }
        if (target.id === "medium") {
            playareaheight = 600;
            playareawidth = 800;
            boxappearingtime = 1000
        }
        if (target.id === "hard") {
            playareaheight = 768;
            playareawidth = 1024;
            boxappearingtime = 750
        }
        $("#play-area").css({
            height: `${playareaheight}` + 'px',
            width: `${playareawidth}` + 'px',
            border: "1px solid black"
        });
    }
    $("#startgame").click(() => {
        startGame(boxappearingtime);
    });
    function startGame(boxappearingtime) {
        timerInterval = setInterval(() => {
            timelimit -= 1;
            $("#timer").text(timelimit);
            if (timelimit === 0) {
                endGame();
            }
        }, 1000)
        randomCirclegenerateInterval = setInterval(() => {
            var randomobject = generateRandomCircle();
            var div = `<div class=${randomobject.className}></div>`
            $("#play-area").html(div);
            var randompos = generateRandomPosition(playareaheight, playareaheight);
            $("." + randomobject.className).css({
                "position": "absolute",
                "border-radius": `${borderRadius}` + "%",
                "left": `${randompos.x}` + "px",
                "top": `${randompos.y}` + "px"
            });
        }, `${boxappearingtime}`);
        var count = 0
        $("#play-area").on("click", "div", (e) => {

            var targetclassname = e.target.className;
            var scoreGained = returnScore(targetclassname);
            score += scoreGained;
            $("#score").html(`<div>score ${score}<div>`);

        })

    }
    function endGame() {
        let gameOver = $("<div id ='over'>Game over</div>");
        $("#heading").after(gameOver);
        clearInterval(timerInterval);
        clearInterval(randomCirclegenerateInterval);
        $("#play-area").html(" ");
    }
    function generateRandomCircle() {
        let circleconfiglength = circleconfig.length;
        let randomCircleIndex = Math.floor(Math.random() * circleconfiglength);
        return circleconfig[randomCircleIndex];
    }
    function generateRandomPosition(playareaheight, playareawidth) {
        let random_x = Math.floor(Math.random() * `${playareawidth}`);
        let random_y = Math.floor(Math.random() * `${playareaheight}`);
        return {
            x: random_x,
            y: random_y
        }
    }
    function returnScore(targetclassname) {
        var clickedcircle = circleconfig.filter((elem) => {
            if (targetclassname === elem.className) {
                return true;
            }
            else {
                return false;
            }
        })
        if (clickedcircle[0] !== null) {
            return clickedcircle[0].score
        }
        else {
            return 0
        }
    }
    return {
        startGame: startGame
    }
}
clickBasedGame();