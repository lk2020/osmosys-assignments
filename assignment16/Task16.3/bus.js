//booking related works
function kaveriTravels() {
    function seatBooking() {
        //logic for selecting a seat
        let payment = 0
        var selected_seats = [];
        $(".seats").on("click", (e) => {
            $(e.target).toggleClass("selected");
            var seat_array = $(".seats");
            selected_seats = selectedSeats(seat_array);
            payment = payment_calculations(selected_seats);
            payNow(payment);
        })
        $("#pay-now").click(() => {
            changeLocation();
        })
    }
    //returns the selected seats
    function selectedSeats(seats) {
        let selected = []
        for (i of seats) {
            if (i.classList.contains("selected")) {
                selected.push(i);
            }
        }
        return selected;
    }
    //payment related tasks
    function payment_calculations(selected_seats) {
        let amount = 900;
        let sum = 0;
        for (i of selected_seats) {
            if (i.classList.contains("window")) {
                sum += (amount + (amount * 10 / 100));
            }
            else {
                sum += amount;
            }
        }
        return sum
    }
    function changeLocation() {
        let link = $("#payment").attr("href");
        location.href = `${link}`;
    }
    function payNow(payment) {
        let remove_prev = $(".pay");
        remove_prev.remove();
        let payment_detail = $("<div class = 'pay'> you have to pay " + `${payment}` + "</div>");
        let date = $("#date");
        date.before(payment_detail);
    }

    return {
        seatBooking: seatBooking
    }
}
// here we are wrtting the code for the payment section
function paymentSection() {
    $("#pay-now").click(() => {
        var seats = bookingSection().seatBooking();
        console.log(seats);
    });
}


var booking_obj = kaveriTravels();
booking_obj.seatBooking();
