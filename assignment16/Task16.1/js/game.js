function clickBasedGame() {
    var timelimit = 60;
    var timeInterval;
    var randomCirclegenerateInterval;
    var circleconfig = circleConfig().scoredata;
    var score = 0;
    function startGame() {
        timelimit = 60;
        timerInterval = setInterval(() => {
            timelimit -= 1;
            $("#timer").text(timelimit);
            if (timelimit === 0) {
                endGame();
            }
        }, 1000)
        randomCirclegenerateInterval = setInterval(() => {
            var randomobject = generateRandomCircle();
            var div = `<div class=${randomobject.className}></div>`
            $("#play-area").html(div);
            var randompos = generateRandomPosition();
            $("." + randomobject.className).css({
                "position": "absolute",
                "left": `${randompos.x}` + "px",
                "top": `${randompos.y}` + "px"
            });
        }, 1000);
        var count = 0
        $("#play-area").on("click", "div", (e) => {

            var targetclassname = e.target.className;
            var scoreGained = returnScore(targetclassname);
            score += scoreGained;
            $("#score").html(`<div>score ${score}<div>`);

        })

    }
    function endGame() {
        let gameOver = $("<div id ='over'>Game over</div>");
        $("#heading").after(gameOver);
        clearInterval(timerInterval);
        clearInterval(randomCirclegenerateInterval);
        $("#play-area").html(" ");
    }
    function generateRandomCircle() {
        let circleconfiglength = circleconfig.length;
        let randomCircleIndex = Math.floor(Math.random() * circleconfiglength);
        return circleconfig[randomCircleIndex];
    }
    function generateRandomPosition() {
        let random_x = Math.floor(Math.random() * 750);
        let random_y = Math.floor(Math.random() * 550);
        return {
            x: random_x,
            y: random_y
        }
    }
    function returnScore(targetclassname) {
        var clickedcircle = circleconfig.filter((elem) => {
            if (targetclassname === elem.className) {
                return true;
            }
            else {
                return false;
            }
        })
        if (clickedcircle[0] !== null) {
            return clickedcircle[0].score
        }
        else {
            return 0
        }
    }
    return {
        startGame: startGame
    }
}
clickBasedGame().startGame();