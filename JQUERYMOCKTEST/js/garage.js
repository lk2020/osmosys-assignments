function garage() {
    var make = $("#make");
    var options = "";
    function addToModelDropdown(target) {
        if (target.value == "maruti") {
            options += "<option>800(1995)</option>" +
                "<option>Alto(2000)</option>" +
                "<option>WagnoR(2002)</option>" +
                "<option>Esteem(2004)</option>" +
                "<option>SX4(2007)</option>"
        }
        if (target.value == "tata") {
            options += "<option>Indica(2001)</option>" +
                "<option>Indigo(2006)</option>" +
                "<option>Safari(2003)</option>" +
                "<option>Sumo(2001)</option>"
        }
        if (target.value == "chevrolet") {
            options += "<option>Beat(2006)</option>" +
                "<option>Travera(2002)</option>" +
                "<option>Spark(2007)</option>"
        }
        if (target.value == "toyota") {
            options += "<option>Camry(2005)</option>" +
                "<option>Etios(2010)</option>" +
                "<option>Corrolla(2003)</option>" +
                "<option>Endeovour(2008)</option>"
        }
        return options;
    }
    make.on("click", (e) => {
        var model = $("#model");
        options = " ";
        model.html(" ")
        options = addToModelDropdown(e.target);
        model.html(options);
    });

    var submit = $("#submit");
    var email = $("#email");
    var contact = $("#contact");
    email.on("focusout", () => {
        validateEmail(email.val());
    })
    contact.on("focusout", () => {
        validateContact(contact.val());
    })
    submit.click((e) => {
        e.preventDefault();
        addCarstoList();
        calculateTotalYear();
        clearfields();
    });
    function validateEmail(target) {
        var reg = /[A-z a-z]+[@][a-z]+[.](com)/;
        if (reg.test(target)) {
            alert("valid emailid")
        } else {
            alert("you hava entered wrong email id")
        }
    }
    function validateContact(target) {
        var reg = /\d{10}/;
        if (reg.test(target)) {
            alert("valid number");
        }
        else {
            alert("incorrect number");
        }
    }
    function addCarstoList() {
        let make = $("#make");
        let model = $("#model");
        let color = $("#color");
        let problem = $("#problems");
        let table = $("#car-list");
        let newrow = $(`<tr></tr>`);
        let years = calculateTotalYear();
        let rowdata = `
        <td>${make.val()}</td>
        <td>${model.val()}</td>
        <td>${color.val()}</td>
        <td>${problem.val()}</td>
        <td>${years}</td>
        <td><input type="checkbox" class="check"></td>
        `
        newrow.append(rowdata);
        table.append(newrow);
    }
    $("#car-list").click((e) => {
        removeCarfromList(e.target);
    });
    function removeCarfromList(target) {
        if (target.classList.contains("check")) {
            target.parentElement.parentElement.remove();
        }
    }
    function calculateTotalYear() {
        let model = $("#model");
        let string = model.val();
        let manufacturingyear = string.slice(-5, -1);
        let currentyear = 2020;
        let years = currentyear - parseInt(manufacturingyear);
        return years;
    }
    function clearfields() {
        let firstname = $("#firstname");
        let lastname = $("#lastname");
        let email = $("#email");
        let contact = $("#contact")
        firstname.val(" ");
        lastname.val(" ");
        email.val(" ");
        contact.val(" ");
    }
}

garage();