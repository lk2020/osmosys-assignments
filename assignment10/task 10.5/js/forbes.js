// Encapsulate the code and increase the maintainability
var companies = {
    "TCS": {
        "revenue": 1000000,
        "expenses": {
            "salaries": 30,
            "rent": 20,
            "utilities": 15
        },
        "employees": [
            {
                "name": "joe",
                "age": 30,
                "role": "Admin"
            },
            {
                "name": "sam",
                "age": 40,
                "role": "Tester"
            },
            {
                "name": "sherlock",
                "age": 45,
                "role": "programmer"
            }
        ]
    },
    "GGk": {
        "revenue": 100000,
        "expenses": {
            "salaries": 35,
            "rent": 20,
            "utilities": 15
        },
        "employees": [
            {
                "name": "sinchan",
                "age": 33,
                "role": "Admin"
            },
            {
                "name": "rajeev",
                "age": 30,
                "role": "Tester"
            },
            {
                "name": "charlie",
                "age": 43,
                "role": "programmer"
            }
        ]
    },
    "osmosys": {
        "revenue": 200000,
        "expenses": {
            "salaries": 32,
            "rent": 24,
            "utilities": 25
        },
        "employees": [
            {
                "name": "ram",
                "age": 28,
                "role": "Admin"
            },
            {
                "name": "sameer",
                "age": 43,
                "role": "Tester"
            },
            {
                "name": "amar",
                "age": 35,
                "role": "programmer"
            }
        ]
    }
}
//printing the name of the employees whose age is less than and greater than x
function lessThanAndGreaterThan(age) {
    var lessthan = []
    var greaterthan = []
    for (i in companies) {
        for (j = 0; j < companies[i].employees.length; j += 1) {
            if (companies[i].employees[j].age < age) {
                lessthan.push(companies[i].employees[j].name)
            }
            else {
                greaterthan.push(companies[i].employees[j].name);
            }
        }
    }
    console.log("The persons whose age less than " + age + " are ", lessthan);
    console.log("The persons whose age greater than " + age + " are ", greaterthan);
}
//printing the youngest and oldest of the company
function findYoungestAndOldest() {
    var arr = []
    for (i in companies) {
        for (j = 0; j < companies[i].employees.length; j += 1) {
            arr.push(companies[i].employees[j].age)
        }
    }
    arr.sort();
    console.log("The age of the youngest employeee is", arr[0]);
    console.log("The age of the oldest employee is ", arr[arr.length - 1]);
}
function findRole() {
    var programmer = [];
    var Tester = [];
    var Admin = [];
    //pushing in to the Admin array
    for (i in companies) {
        for (j = 0; j < companies[i].employees.length; j += 1) {
            if (companies[i].employees[j].role == "Admin") {
                Admin.push(companies[i].employees[j].name);
            } else if (companies[i].employees[j].role == "Tester") {
                Tester.push(companies[i].employees[j].name)
            } else {
                programmer.push(companies[i].employees[j].name)
            }
        }
    }
    console.log("The people having the role of programmer are", programmer);
    console.log("The people having the role of Tester are", Tester);
    console.log("The people having the role of Admin are", Admin);
}
function findProfit() {
    let profits = []
    let max = 0;
    cnames = []
    for (let i in companies) {
        cnames.push(i);
    }
    for (i in companies) {
        let expense = 0
        for (j in companies[i].expenses) {
            expense += companies[i].revenue * (companies[i].expenses[j] / 100)
        }
        console.log("profit of " + i + " is ", (companies[i].revenue - expense))
        profits.push(companies[i].revenue - expense);
    }
    for (let i of profits) {
        if (i > max) {
            max = i;
        }
    }
    for (let i in profits) {
        if (profits[i] == max) {
            console.log(`${cnames[i]} earning maximum profit of ${max}`);
        }
    }
}
var age = prompt("pass the age condition");
lessThanAndGreaterThan(age);
findYoungestAndOldest();
findRole();
findProfit();